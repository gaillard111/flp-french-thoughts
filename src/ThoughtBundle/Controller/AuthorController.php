<?php

namespace ThoughtBundle\Controller;

use FOS\ElasticaBundle\Finder\TransformedFinder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ThoughtBundle\Model\AuthorModel;

/**
 * Class AuthorController
 *
 * @package ThoughtBundle\Controller
 */
class AuthorController extends Controller
{
    const AUTHOR_FIRST_LETTER = 'A';

    /**
     * @Route("/authors-list", methods={"GET"}, options={"sitemap" = true})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $start = microtime(true);

        $page = $request->query->getInt('page', 1);

        $alpha = $request->query->getAlpha('alpha', 1);

        $countItem = 60;

        /** @var TransformedFinder $authorsFinder */
        $authorsFinder = $this->container->get('fos_elastica.finder.app.author');

        $paginator = $this->get('knp_paginator');

        if (!$alpha) {
            $alpha = self::AUTHOR_FIRST_LETTER;
        }

        /** @var AuthorModel $authorModel */
        $authorModel = $this->container->get('thought.model.author_model');

        $authors = $authorModel->getAuthorsByStringStartElastic($alpha, $authorsFinder);

        $pagination = $paginator->paginate(
            $authors,
            $page,
            $countItem
        );

        $timeExecute = microtime(true) - $start;

        return $this->render('ThoughtBundle::author.html.twig', [
            'authors'     => $pagination,
            'timeExecute' => $timeExecute,
        ]);
    }
}
